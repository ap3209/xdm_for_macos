# XDM_for_MacOS

<p>This is simple script which will download xdm dependency files, make app and install the app on application folder automatically.</p>

<p>The script use the xdman.jar which is provided by developer.</p>

<h2>Official XDM github page:- 
<a href="https://github.com/subhra74/xdm">
Link</a> </h2>

<h2>How to run this script:-</h2>
<p>% git clone https://gitlab.com/ap3209/xdm_for_macos.git</p>
<br>
<p>% cd xdm_for_macos && chmod +x install.sh</p>
<br>
<p>% ./install.sh</p>

<br>

<p>Nate1:- To automatic start xdm on every boot. make sure "Launch XDM when system starts" is checked on.</p>
<p>Tools>Options>Advanced settings</p>

<br>

<p>Note2:- This script is tested on MacOS Monterey and High Sierra. It should work on High Sierra+ OSs</p>

<br>

<h2>Credits</h2>
<ul>

  <li><a href="https://github.com/subhra74">subhra74</a> for XDM</li>

</ul>

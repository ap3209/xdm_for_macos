#!/bin/sh

var1="$PWD"

# Download File:
#curl -o $var1/xdman.jar https://github.com/subhra74/xdm/releases/download/7.2.11/xdman.jar

curl -o $var1/java17.tar.gz https://download.oracle.com/java/17/latest/jdk-17_macos-x64_bin.tar.gz

curl -o $var1/youtube-dl https://github.com/ytdl-org/youtube-dl/releases/download/2021.12.17/youtube-dl

curl -o $var1/ffmpeg.zip https://evermeet.cx/ffmpeg/getrelease/zip -JL

# Extract Files and Permission:
tar -xf java17.tar.gz

tar -xf ffmpeg.zip

chmod +x youtube-dl ffmpeg

chmod +x $var1/files/xdman

# build process:
mkdir -p $var1/XDM.app/Contents/MacOS

mkdir -p $var1/XDM.app/Contents/Resources

cp $var1/files/icon.icns $var1/XDM.app/Contents/Resources

mv $var1/jdk-*.jdk $var1/XDM.app/Contents/MacOS

cp $var1/files/xdman.jar $var1/XDM.app/Contents/MacOS/jdk-*.jdk/Contents/Home/bin

mv $var1/youtube-dl $var1/XDM.app/Contents/MacOS/jdk-*.jdk/Contents/Home/bin

mv $var1/ffmpeg $var1/XDM.app/Contents/MacOS/jdk-*.jdk/Contents/Home/bin

cp $var1/files/xdman $var1/XDM.app/Contents/MacOS

cp $var1/files/info.plist $var1/XDM.app/Contents

mv $var1/XDM.app /Applications

echo "Installation is completed...see XDM app in Launchpad"
